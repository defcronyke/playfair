#!/bin/sh
#
# filepass.sh
#
# Version 1.0
#
# This will use part of the daily front page image on cracked.com
# to generate a passphrase that you can use with the playfair
# cipher program. Instead of giving someone the passphrase,
# you can just give them these commands to run, which will
# retrieve the daily passphrase. The point is that the passphrase
# will automatically change every day, when the front page of
# cracked.com gets updated.
#
# You can easily modify this script to use different parts of 
# different pictures from different websites.
#
# Obviously, if you don't save your passphrase anywhere,
# your encrypted message will expire after about a day.
#
# If you want to save a local copy of the picture from which you are
# generating a key, run filepass.sh with the -s flag and the picture
# will be saved to the path specified in the variable picturedir
# below.
#
# If you want to generate a key from a local picture (or any other
# type of file), run filepass.sh with the -l flag, followed by the
# file you would like to generate the key from.
#_________________________________________________________________________
#
# Quickstart - to use this script with the playfair program:
#
# make sure it's executable: chmod 755 filepass.sh
# copy it into your path (as root): cp -a filepass.sh /usr/bin
#
#
# To encrypt, run playfair like this:
#
#   playfair "`filepass.sh`" "message to encrypt"
#
#
# To decrypt:
#
#   playfair -d "`filepass.sh`" "ewhausidxzqybopman"
#
#
# To encrypt and save the picture that's being used to generage the
# passphrase:
# 
#   playfair "`filepass.sh -s`" "message to encrypt"
#
#
# To encrypt using any local file:
# 
#   playfair "`filepass.sh -l /path/to/any/file`" "message to encrypt"\
#
#
# To decrypt using any local file:
#
#   playfair -d "`filepass.sh -l /path/to/any/file`" "ewhausidxzqybopman"
#_________________________________________________________________________
#
# Copyright Defcronyke Webmaster 2011
#
# You can copy and modify this script for free, but you can never 
# sell it. All modified or unmodified versions of this script must 
# remain completely free and they all must retain this paragraph as 
# well as the Copyright notice above. If this script breaks your shit,
# you're fucked and it's not my problem.
#_________________________________________________________________________
picturedir="$HOME"
picturepath="$picturedir/.filepass_`date '+%F_%H%M%S'`.jpg"

if [ "$1" = "-s" ]; then
  pictureremotepath=`links -source http://cracked.com/ | gunzip | grep "phpimages/article" | head -1 | cut -d"\"" -f2`
  links -source "$pictureremotepath" | gunzip > "$picturepath"
  chmod 700 "$picturepath"
  picturekey=`cat -vet "$picturepath" | tail -3 | head -1 | sed "s/'//g" | sed "s/\"//g"`
  echo "$picturekey"
elif [ "$1" = "-l" ]; then
  picturekey=`cat -vet "$2" | tail -3 | head -1 | sed "s/'//g" | sed "s/\"//g"`
  echo "$picturekey"
else
  pictureremotepath=`links -source http://cracked.com/ | gunzip | grep "phpimages/article" | head -1 | cut -d"\"" -f2`
  picturekey=`links -source "$pictureremotepath" | gunzip | cat -vet | tail -3 | head -1 | sed "s/'//g" | sed "s/\"//g"`
  echo "$picturekey"
fi

