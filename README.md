The README is here: [https://gitlab.com/defcronyke/playfair/snippets/1943470](https://gitlab.com/defcronyke/playfair/snippets/1943470)

Online version: [https://bit.ly/playfair-online](https://bit.ly/playfair-online)  

Info about the Playfair cipher: [https://en.wikipedia.org/wiki/Playfair_cipher](https://en.wikipedia.org/wiki/Playfair_cipher)
