//
//  playfair - A C++ implementation of the playfair cipher.
//             Playfair encryption was used by the British forces
//             during the Second Boer War and World War I.
//
//             With this program you can encrypt and decrypt
//             messages using a passphrase. The message can only
//             contain letters. All other characters will be
//             removed. Sometimes the letter x will be added,
//             also the letter j will be converted to i.
//             This is not a bug, it's part of how playfair is
//             supposed to work.
//
//             Tested and working on Linux and windows.
//
// Copyright Defcronyke Webmaster 2011
//
// You can copy and modify this program for free, but you can never
// sell it. All modified or unmodified versions of this program must
// remain completely free, full source code must be freely available,
// and they all must retain this paragraph as well as the copyright
// notice above. If this program breaks your shit, you're fucked and
// it's not my problem.
//
// Check http://eternalvoid.net/code/playfair/ for updated versions
// of this program, to report bugs, or to give any feedback.
// 
////

#include <iostream>
#include <string>
#include <sstream>
#include <boost/xpressive/xpressive.hpp>

using namespace boost::xpressive;
using namespace std;

int verbose_switch = 0;

void usage(char**& argv)
{
    cout << endl
         << "usage: playfair [ -d | -v ] 'passphrase' 'message to encrypt/decrypt'" << endl
         << endl
         << "       -d  decrypt     without this the default action is encrypt" << endl
         << "       -v  verbose" << endl << endl;
}

//  Encryption Section
//__________________________________________________________
string* genKey(string& pass)
{
    int j = 0;
    int count = 0;
    stringstream ss[5];
    string * key = new string[5];
    string alphabet[2] = {"abcdefghiklmnopqrstuvwxyz",
                          "0000000000000000000000000"};
    for (int i = 0; i < pass.length(); i++)
    {
        int letterpos = 0;

        if (pass[i] == 'j')
        {
            pass[i] = 'i';
        }

        letterpos = alphabet[0].find(pass[i]);

        if (alphabet[1][letterpos] == '0')
        {
            alphabet[1][letterpos] = '1';
            ss[count] << pass[i];

            if (j >= 4)
            {
                count++;
                j = 0;
            }
            else
            {
                 j++;
            }
        }
    }

    for (int i = 0; i < alphabet[0].length(); i++)
    {
        if (alphabet[1][i] == '0')
        {
            ss[count] << alphabet[0][i];

            if (j >= 4)
            {
                count++;
                j = 0;
            }
            else
            {
                j++;
            }
        }
    }

    for (int i = 0; i < 5; i++)     // copy buffer to return variable
    {
        ss[i] >> key[i];
    }

    if (verbose_switch == 1)        // Display alphabet array
    {
        cout << endl << "Letters used in passphrase:" << endl << endl << alphabet[0] << endl << alphabet[1] << endl;
    }

    return key;
}

void cubify(string * text)
{
    if (verbose_switch == 1)
    {
        cout << endl << "Okay, this is your key:" << endl << endl
             << "    0 1 2 3 4" << endl << " " << endl;

        for (int i = 0; i < 5; i++)
        {
            cout << i << "   ";

            for (int k = 0; k < 5; k++)
            {
                cout << text[i][k] << " ";
            }
            cout << endl;
        }
    }
}

string genDiagram(string message)
{
    string diagram;
    stringstream ss;

    for (int i = 0; i < message.length(); i++)
    {
        if (message[i] == 'j')
        {
            message[i] = 'i';
        }

        if (message[i+1] == 'j')
        {
            message[i+1] = 'i';
        }

        if (message[i+2] == 'j')
        {
            message[i+2] = 'i';
        }

        if (message[i] == ' ')
        {
            message.erase(i, 1);
        }

        if (message[i+1] == ' ')
        {
            message.erase(i+1, 1);
        }

        if (message[i+1] == message[i] || message[i] == 'x')
        {
            ss << message[i] << "x";
        }
        else
        {
            if (!message[i+1])
            {
                ss << message[i];
            }
            else
            {
                ss << message[i] << message[i+1];
                i++;
            }
        }
    }

    if ((ss.str().length() % 2))
    {
        ss << "x";
    }
    ss >> diagram;

    return diagram;
}

void printDiagram(string diagram)
{
    if (verbose_switch == 1)
    {

        int diagram_length = (diagram.length() / 2);
        int diagram_number = 0;
        int fourteen_counter = 0;
        int abc123 = 0;

        for (int i = 0; diagram_number < diagram_length;)
        {
            if (fourteen_counter < 14)
            {
                if (abc123 == 0)
                {
                    if (i <= diagram.length()-1)
                    {
                        cout << diagram[i];
                    }
                    if (i+1 <= diagram.length()-1)
                    {
                       cout << diagram[i+1];
                    }

                    cout << "   ";

                    fourteen_counter++;
                    i+=2;
                }
                else
                {
                    if (diagram_number+1 < 10)
                    {
                        cout << diagram_number+1 << "    ";
                    }
                    else if (diagram_number+1 < 100)
                    {
                        cout << diagram_number+1 << "   ";
                    }
                    else if (diagram_number+1 < 1000)
                    {
                        cout << diagram_number+1 << "  ";
                    }
                    else if (diagram_number+1 < 10000)
                    {
                        cout << diagram_number+1 << " ";
                    }
                    else if (diagram_number+1 < 100000)
                    {
                        cout << diagram_number+1;
                    }
                    else
                    {
                        cout << "??" << "   ";
                    }

                    diagram_number++;
                    fourteen_counter++;
                }
            }
            else if (fourteen_counter >= 14)
            {
                fourteen_counter = 0;

                if (abc123 == 0)
                {
                    abc123 = 1;
                    cout << endl;
                }
                else
                {
                    abc123 = 0;
                    cout << endl << endl;
                }
            }
        }
        cout << endl;
    }
}

string encMsg(string * key, string diagram)
{
    stringstream ss;
    string encodedmsg;
    int pos[(diagram.length()/2) + 1][2][2];
    int newpos[(diagram.length()/2) + 1][2][2];

    for (int i = 0, m = 0, n = 0; i < diagram.length(); i++)    // for every letter in the diagram
    {
        for (int k = 0; k < 5; k++)                             // we have 5 rows in the key square
        {
            if (key[k].find(diagram[i]) != -1)                  // if we find a match in row
            {
                pos[m][n][0] = k;                               // save row number in matrix
                pos[m][n][1] = key[k].find(diagram[i]);         // save column number in matrix

                if (n == 0)
                {
                    n = 1;
                }
                else
                {
                    n = 0;
                }

                if (i % 2 != 0)
                {
                    m++;
                }
            }
        }
    }

    if (verbose_switch == 1)    // Print diagram matrix
    {
        cout << endl << "Here are the coordinates of each diagram letter's position in the key.\n"
                        "The top-left corner of your key is 0,0 (row,column):" << endl << endl;
        for (int x = 0, w = 4; x < (diagram.length()/2); x++)
        {
            for (int y = 0; y < 2; y++)
            {
                for (int z = 0; z < 2; z++)
                {
                    if (w > 3)
                    {
                        w = 0;

                        if (x < 9)
                        {
                            cout << x+1 << ".    ";
                        }
                        else if (x < 99)
                        {
                            cout << x+1 << ".   ";
                        }
                        else if (x < 999)
                        {
                            cout << x+1 << ".  ";
                        }
                        else
                        {
                            cout << x+1 << ". ";
                        }
                    }

                    if (w == 0 || w == 2)
                    {
                        cout << pos[x][y][z] << ",";
                        w++;
                    }
                    else if (w == 1)
                    {
                        cout << pos[x][y][z] << " ";
                        w++;
                    }
                    else
                    {
                        cout << pos[x][y][z] << endl;
                        w++;
                    }
                }
            }
        }
    }

    for (int i = 0; i < (diagram.length()/2); i++)  // Begin encryption
    {
        // Rule 1
        // if the two letters are in the same row
        // lookup key to find letter to the right of each letter
        // and save result in stringstream
        if (pos[i][0][0] == pos[i][1][0])
        {
            if (pos[i][0][1] + 1 <= 4)
            {
                ss << key[pos[i][0][0]][pos[i][0][1] + 1];
            }
            else
            {
                ss << key[pos[i][0][0]][pos[i][0][1] - 4];
            }

            if (pos[i][1][1] + 1 <= 4)
            {
                ss << key[pos[i][1][0]][pos[i][1][1] + 1];
            }
            else
            {
                ss << key[pos[i][1][0]][pos[i][1][1] - 4];
            }
        }

        // Rule 2
        // if the two letters are in the same column
        // lookup key to find the letter below each letter
        // and save result in stringstream
        if (pos[i][0][1] == pos[i][1][1])
        {
            if (pos[i][0][0] + 1 <= 4)
            {
                ss << key[pos[i][0][0] + 1][pos[i][0][1]];
            }
            else
            {
                ss << key[pos[i][0][0] - 4][pos[i][0][1]];
            }

            if (pos[i][1][0] + 1 <= 4)
            {
                ss << key[pos[i][1][0] + 1][pos[i][1][1]];
            }
            else
            {
                ss << key[pos[i][1][0] - 4][pos[i][1][1]];
            }
        }

        // Rule 3
        // if the two letters are neither in the same row or the same column
        if (pos[i][0][0] != pos[i][1][0] && pos[i][0][1] != pos[i][1][1])
        {

                ss << key[pos[i][0][0]][pos[i][0][1] + (pos[i][1][1] - pos[i][0][1])]
                   << key[pos[i][1][0]][pos[i][1][1] - (pos[i][1][1] - pos[i][0][1])];
        }
    }
    ss >> encodedmsg;   // copy stringstream to return variable
    return encodedmsg;
}
//_____________________________________________________________________________________________
//  End of Encryption Section



//  Decryption Section
//______________________________________________________________________________________________

string genDecDiagram(string message)
{
    string diagram("\0");
    stringstream ss;

    for (int i = 0; i < message.length(); i++)
    {
        if (message[i] == 'j')
        {
            cout << endl << "error: encrypted message is corrupted" << endl << endl;
        }
        else if (message[i] == ' ')
        {
            cout << endl << "error: encrypted message is corrupted" << endl << endl;
        }
        else
        {
            ss << message[i] << message[i+1];
        }
        i++;
    }

    ss >> diagram;

    return diagram;
}

string decMsg(string * key, string diagram)
{
    stringstream ss;
    string decodedmsg;
    int pos[(diagram.length()/2) + 1][2][2];

    for (int i = 0, m = 0, n = 0; i < diagram.length(); i++)  // for every letter in the diagram
    {
        for (int k = 0; k < 5; k++) // we have 5 rows in the key square
        {
            if (key[k].find(diagram[i]) != -1)    // if we find a match in row
            {
                pos[m][n][0] = k;   // save row number in matrix
                pos[m][n][1] = key[k].find(diagram[i]); // save column number in matrix

                if (n == 0)
                {
                    n = 1;
                }
                else
                {
                    n = 0;
                }

                if (i % 2 != 0)
                {
                    m++;
                }
            }
        }
    }


    if (verbose_switch == 1)    // Print diagram matrix
    {
        cout << endl << "Here are the coordinates of each diagram letter's position in the key.\n"
                        "The top-left corner of your key is 0,0 (row,column):" << endl << endl;
        for (int x = 0, w = 4; x < (diagram.length()/2); x++)
        {
            for (int y = 0; y < 2; y++)
            {
                for (int z = 0; z < 2; z++)
                {
                    if (w > 3)
                    {
                        w = 0;

                        if (x < 9)
                        {
                            cout << x+1 << ".    ";
                        }
                        else if (x < 99)
                        {
                            cout << x+1 << ".   ";
                        }
                        else if (x < 999)
                        {
                            cout << x+1 << ".  ";
                        }
                        else
                        {
                            cout << x+1 << ". ";
                        }
                    }

                    if (w == 0 || w == 2)
                    {
                        cout << pos[x][y][z] << ",";
                        w++;
                    }
                    else if (w == 1)
                    {
                        cout << pos[x][y][z] << " ";
                        w++;
                    }
                    else
                    {
                        cout << pos[x][y][z] << endl;
                        w++;
                    }
                }
            }
        }
    }

    for (int i = 0; i < (diagram.length()/2); i++)  // Begin decryption
    {
        // Rule 1
        // if the two letters are in the same row
        // lookup key to find letter to the left of each letter
        // and save result in stringstream
        if (pos[i][0][0] == pos[i][1][0])
        {
            if (pos[i][0][1] - 1 >= 0)
            {
                ss << key[pos[i][0][0]][pos[i][0][1] - 1];
            }
            else
            {
                ss << key[pos[i][0][0]][pos[i][0][1] + 4];
            }

            if (pos[i][1][1] - 1 >= 0)
            {
                ss << key[pos[i][1][0]][pos[i][1][1] - 1];
            }
            else
            {
                ss << key[pos[i][1][0]][pos[i][1][1] + 4];
            }
        }

        // Rule 2
        // if the two letters are in the same column
        // lookup key to find the letter below each letter
        // and save result in stringstream
        if (pos[i][0][1] == pos[i][1][1])
        {
            if (pos[i][0][0] - 1 >= 0)
            {
                ss << key[pos[i][0][0] - 1][pos[i][0][1]];
            }
            else
            {
                ss << key[pos[i][0][0] + 4][pos[i][0][1]];
            }

            if (pos[i][1][0] - 1 >= 0)
            {
                ss << key[pos[i][1][0] - 1][pos[i][1][1]];
            }
            else
            {
                ss << key[pos[i][1][0] + 4][pos[i][1][1]];
            }
        }

        // Rule 3
        // if the two letters are neither in the same row or the same column
        if (pos[i][0][0] != pos[i][1][0] && pos[i][0][1] != pos[i][1][1])
        {

                ss << key[pos[i][0][0]][pos[i][0][1] + (pos[i][1][1] - pos[i][0][1])]
                   << key[pos[i][1][0]][pos[i][1][1] - (pos[i][1][1] - pos[i][0][1])];
        }
    }
    ss >> decodedmsg;   // copy strinstream to return variable

    return decodedmsg;
}

//______________________________________________________________________________________________
// End of Decrytion Section


int main(int argc, char** argv)
{
    string passphrase = "\0";
    string* key = NULL;
    string message = "\0";
    string diagram = "\0";
    string result = "\0";
    stringstream ss;

    int decryption_switch = 0;

// interactive passphrase input
//    cout << "What passphrase would you like to use? ";
//    getline(cin, passphrase);

    if (argc == 3)  // if two args
    {
        passphrase = string(argv[1]);
        message = string(argv[2]);
    }
    else if (argc == 4) // if three args
    {
        passphrase = string(argv[2]);
        message = string(argv[3]);

        if (!strcmp(argv[1], "-v"))         // if first arg is -v (verbose mode)
        {
            verbose_switch = 1;
        }
        else if (!strcmp(argv[1], "-d"))    // if first arg is -d (decryption mode)
        {
            decryption_switch = 1;
        }
        else if (!strcmp(argv[1], "-vd") || !strcmp(argv[1], "-dv"))    // if first arg is verbose and decrypt
        {
            verbose_switch = 1;
            decryption_switch = 1;
        }
        else
        {
            cout << endl
                 << "error: " << argv[1] << " is an incorrect switch" << endl;
            usage(argv);

            return -1;
        }
    }
    else if (argc == 5)     // if four args
    {
        passphrase = string(argv[3]);
        message = string(argv[4]);

        if (!strcmp(argv[1], "-v") || !strcmp(argv[2], "-v"))
        {
            verbose_switch = 1;
        }
        if (!strcmp(argv[1], "-d") || !strcmp(argv[2], "-d"))
        {
            decryption_switch = 1;
        }
        if (strcmp(argv[1], "-v") && strcmp(argv[2], "-v"))
        {
            cout << endl
                 << "error: incorrect switch" << endl;
            usage(argv);

            return -1;
        }
        if (strcmp(argv[1], "-d") && strcmp(argv[2], "-d"))
        {
            cout << endl
                 << "error: incorrect switch" << endl;
            usage(argv);

            return -1;
        }
    }
    else
    {
        usage(argv);

        return -1;
    }

    string fixedpass = "\0";
    string fixedmess = "\0";

    sregex rex = sregex::compile( "[A-Za-z ]+" );

    sregex_iterator cur( passphrase.begin(), passphrase.end(), rex );
    sregex_iterator end;

    for( ; cur != end; ++cur )
    {
        smatch const &what = *cur;
        fixedpass.append(what[0]);
    }

    std::transform(fixedpass.begin(), fixedpass.end(),
    fixedpass.begin(), ::tolower);

    for (int i = 0; i < fixedpass.length(); i++)
    {
        if (fixedpass[i] == ' ' && fixedpass[i+1] == ' ')
        {
            fixedpass.erase(i+1, 1);
        }
    }

    sregex_iterator cur2( message.begin(), message.end(), rex );
    sregex_iterator end2;

    for( ; cur2 != end2; ++cur2 )
    {
        smatch const &what = *cur2;
        fixedmess.append(what[0]);
    }

    std::transform(fixedmess.begin(), fixedmess.end(),
    fixedmess.begin(), ::tolower);

    for (int i = 0; i < fixedmess.length(); i++)
    {
        if (fixedmess[i] == ' ' && fixedmess[i+1] == ' ')
        {
            fixedmess.erase(i+1, 1);
        }
    }

    key = genKey(fixedpass);
    cubify(key);

// interactive message input
//    cout << endl << "Please type the message you wish to encrypt:" << endl << endl;
//    getline(cin, message);

    if (decryption_switch == 0)
    {
        diagram = genDiagram(fixedmess);
    }
    else if (decryption_switch == 1)
    {
        diagram = genDecDiagram(fixedmess);
    }

    if (verbose_switch == 1)
    {
        cout << endl << "This is your message diagram:" << endl << endl;
    }

    printDiagram(diagram);

    if (decryption_switch == 0)
    {
        result = encMsg(key, diagram);
    }
    else if (decryption_switch == 1)
    {
        result = decMsg(key, diagram);
    }

    if (verbose_switch == 1)
    {
        if (decryption_switch == 0)
        {
            cout << endl << "This is your encrypted message:" << endl << endl;
        }
        else
        {
            cout << endl << "This is your decrypted message:" << endl << endl;
        }
    }

    cout << result << endl;

    if (verbose_switch == 1)
    {
        cout << endl;
    }

    return 0;       // end encryption
}
